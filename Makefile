# Makefile
# SITES list every directory we have to process. TOPTARGETS is a list of 
# supported targets for Makefile
#   'all' target MUST be defined inside subdir/Makefile. Is a list of final 
#         processed files. See ela-lin.net/Makefile
#   'deps' target MUST NOT be defined in subdir/Makefile. See Makefile.in
#   'clean' target MUST NOT be defined in subdir/Makefile. See Makfile.in
SITES := ela-lin.net gab-maz.com
# Nothing to touch below this line
TOPTARGETS := all deps clean
# General dependencies. Hard to explain.
# If you run 'make <targetname>' in root directory, this dependencies will
# run 'make <targetname>' in each subdirecory (each entry in SITES)
$(TOPTARGETS): $(SITES)
$(SITES):
	$(MAKE) -C $@ $(MAKECMDGOALS)
.PHONY: $(TOPTARGETS) $(SITES)
#End Makefile