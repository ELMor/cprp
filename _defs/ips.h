#include "macros.h"
#include "networks.h"
//B4 de IPs, servidores conocidos
#define WMNET_B4ROUTER                  2
#define WMNET_B4MAINLDAP                10
#define WMNET_B4KERB                    12
#define WMNET_B4LDAP1                   24
#define WMNET_B4LDAP2                   25
#define WMNET_B4NFS                     32

//IPs, servidores conocidos
#define WMNET_BUILDIP(b2pb3p,b4)        __CONCAT(__CONCAT(__CONCAT(WMNET_B1IP,.),b2pb3p),__CONCAT(.,b4))

#define WMNET_IPROUTER                  WMNET_BUILDIP(WMNET_B23IP,WMNET_B4ROUTER)
#define WMNET_IPMAINLDAP                WMNET_BUILDIP(WMNET_B23IP,WMNET_B4MAINLDAP)
#define WMNET_IPKERB                    WMNET_BUILDIP(WMNET_B23IP,WMNET_B4KERB)
#define WMNET_IPLDAP1                   WMNET_BUILDIP(WMNET_B23IP,WMNET_B4LDAP1)
#define WMNET_IPLDAP2                   WMNET_BUILDIP(WMNET_B23IP,WMNET_B4LDAP2)
#define WMNET_IPNFS                     WMNET_BUILDIP(WMNET_B23IP,WMNET_B4NFS)
