#include "site.h"
/*
    Esta es una prueba de sustitición de macros. 
    Se usa el preprocesador de C para sustituir las macros. 
    https://es.wikipedia.org/wiki/Preprocesador_de_C

*/

Dentro de este archivo se pueden usar las macros definidas en los includes (#include)
Visual Studio Code ayuda a escribirlas:

Por ejemplo:

echo WMNET_IPLDAP1 >> /etc/hosts
echo WMNET_B23IP

//Para preprocesar este archivo se emplea la primera fase del compilador de C, el preprocesador
/*
    cc <filename> -E -P -I${workspaceroot} -o outfile

    Donde:
        -E indica que solo se proceda a sustituir macros. <filename> debe tener la extension '.c'
        -P Indica que no se emita informacion de transformación de números de línea.
        -I Indica el directorio raíz del árbol de includes. 
        -C No descartar comentarios
        -o Nombre del archivo de salida. Si se omite, se usa la consola

        WMNET_IPLDAP1 -> Los comentarios no se tratan

    El directorio raíz de -I indica root en  #include <root/subdir/file.h>

    Ejemplo:
    cc test.sh.c -E -P -I..

    Devuelve:
        Dentro de este archivo se pueden usar las macros definidas en los includes (#include)
        Visual Studio Code ayuda a escribirlas:

        Por ejemplo:

        echo 172.23.0.24 >> /etc/hosts
        echo 23.0.
*/